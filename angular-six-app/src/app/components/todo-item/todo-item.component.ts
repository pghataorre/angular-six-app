import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: "app-todo-item",
  templateUrl: "./todo-item.component.html",
  styleUrls: ["./todo-item.component.scss"]
})
export class TodoItemComponent implements OnInit {
  @Input() todoItem: Todo[];

  constructor() {}

  ngOnInit() {}

  setClasses() {
    let classes = {
      todoItem: true,
      complete: this.todoItem.completed
    };

    return classes;
  }

  isCompleted(todoItem) {
    todoItem.completed = !todoItem.completed;
  }

  deleteItem(todoItem) {
    debugger;
  }
}
