import { Component, OnInit } from '@angular/core';
import { importExpr } from '@angular/compiler/src/output/output_ast';
import { TodoService } from '../../services/todo.service';

@Component({
  selector: "app-todo",
  templateUrl: "./todo.component.html",
  styleUrls: ["./todo.component.scss"]
})
export class TodoComponent implements OnInit {
  todos: any[];

  constructor(private todoService:TodoService) {

  }

  ngOnInit() {
    this.todos = this.todoService.getToDolist();
  }
}
