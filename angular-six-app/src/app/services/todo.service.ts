import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  constructor() { }

  getToDolist() {

    return [
      {
        id: 1,
        title: 'To do  1',
        completed: false
      },
      {
        id: 2,
        title: 'To do  2',
        completed: false
      },
      {
        id: 3,
        title: 'To do  3',
        completed: false
      },
      {
        id: 3,
        title: 'To do  4',
        completed: false
      },
      {
        id: 3,
        title: 'To do  5',
        completed: false
      }
    ];

  };
}
